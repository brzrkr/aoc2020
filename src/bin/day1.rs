use std::fs;

fn main() {
    let contents =
        fs::read_to_string("inputs/input1.txt").expect("Couldn't read the file. Cringe.");
    let inputs = contents.split('\n').filter_map(|x| x.parse::<u32>().ok());

    let mut result: u32 = 0;
    for (i, num) in inputs.clone().enumerate() {
        for (j, other) in inputs.clone().enumerate() {
            if i == j || num + other != 2020 {
                continue;
            }

            result = num * other;
        }
    }

    if result == 0 {
        println!("Something went wrong.")
    } else {
        println!("The result is {}", result)
    }
}
