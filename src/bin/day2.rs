use regex::Regex;
use std::fs;

fn main() {
    let contents =
        fs::read_to_string("inputs/input2.txt").expect("Couldn't read the file. Cringe.");
    let inputs = contents.split('\n');

    let pattern = Regex::new("([0-9]+)-([0-9]+) ([a-z]): ([a-z]+)").unwrap();

    let mut correct = 0;
    for input in inputs.into_iter() {
        match pattern.captures(input) {
            None => continue,
            Some(mtch) => {
                let min = mtch.get(1).unwrap().as_str().parse::<usize>().unwrap();
                let max = mtch.get(2).unwrap().as_str().parse::<usize>().unwrap();
                let char = mtch.get(3).unwrap().as_str();
                let password = mtch.get(4).unwrap().as_str();

                let occurrences = password.matches(char).count();

                if occurrences >= min && occurrences <= max {
                    correct += 1
                }
            }
        }
    }

    println!("There are {} correct passwords.", correct)
}
