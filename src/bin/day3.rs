use std::fs;

fn main() {
    let contents =
        fs::read_to_string("inputs/input3.txt").expect("Couldn't read the file. Cringe.");
    let inputs = contents
        .split('\n')
        .map(|line| line.chars().collect::<Vec<char>>())
        .filter(|line| !line.is_empty())
        .collect::<Vec<Vec<char>>>();

    let row_len = inputs[0].len();
    let mut col = 3;
    let mut trees = 0;
    for row in inputs {
        col %= row_len;

        if let '#' = row[col] { trees += 1 }

        col += 3;
    }

    println!("We encountered {} trees.", trees);
}
