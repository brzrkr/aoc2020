use std::collections::HashSet;
use std::fs;

fn main() {
    let contents =
        fs::read_to_string("inputs/input4.txt").expect("Couldn't read the file. Cringe.");
    let inputs = contents.split('\n');

    let expected_fields = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]
        .iter()
        .cloned()
        .collect();

    let mut valid = 0;
    let mut current_fields_found = HashSet::new();
    for line in inputs {
        if line.trim().is_empty() {
            if current_fields_found.is_superset(&expected_fields) {
                valid += 1;
            }

            current_fields_found.clear();
            continue;
        }

        line.split(' ').for_each(|part| {
            current_fields_found.insert(part.split(':').collect::<Vec<&str>>()[0]);
        });
    }

    println!("{} valid passports found.", valid);
}
