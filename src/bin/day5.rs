use std::cmp::max;
use std::fs;

fn binary_part(steps: &str, mut upper_bound: i32, options: (char, char)) -> i32 {
    let mut lower_bound = 0;
    let mut result = 0;

    for step in steps.chars() {
        let diff = ((upper_bound - lower_bound) as f32 / 2.0).ceil() as i32;

        match step {
            _ if step == options.0 => {
                // Take upper half
                lower_bound += diff;
                result = upper_bound;
            }
            _ if step == options.1 => {
                // Take lower half
                upper_bound -= diff;
                result = lower_bound;
            }
            _ => {
                panic!("Fuck.")
            }
        }
    }

    return result;
}

fn main() {
    let contents =
        fs::read_to_string("inputs/input5.txt").expect("Couldn't read the file. Cringe.");
    let inputs = contents
        .split('\n')
        .filter(|line| !line.is_empty())
        .map(|line| (&line[..7], &line[7..]));

    let mut maximum_seat_id = 0;
    for (row_steps, col_steps) in inputs {
        let row_number = binary_part(row_steps, 127, ('B', 'F'));
        let col_number = binary_part(col_steps, 7, ('R', 'L'));

        let seat_id = row_number * 8 + col_number;
        maximum_seat_id = max(maximum_seat_id, seat_id);
    }

    println!("The highest seat ID is {}.", maximum_seat_id);
}
