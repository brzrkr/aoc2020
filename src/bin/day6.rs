use std::fs;
use std::collections::HashSet;


fn main() {
    let contents =
        fs::read_to_string("inputs/input6.txt").expect("Couldn't read the file. Cringe.");
    let inputs = contents
        .split('\n');

    let mut total_answered = 0;
    let mut group_answers: HashSet<char> = HashSet::new();
    for line in inputs {
        if line.is_empty() {
            total_answered += group_answers.len();
            group_answers.clear();
            continue;
        }

        for ch in line.chars() {
            group_answers.insert(ch);
        }
    }

    println!("In total, {} questions have been answered by each group.", total_answered);
}
